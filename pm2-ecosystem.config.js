// Target server hostname or IP address
const TARGET_SERVER_HOST = ['13.233.134.235'];
// Target server username
const TARGET_SERVER_USER = process.env.TARGET_SERVER_USER ? process.env.TARGET_SERVER_USER.trim() : 'ubuntu';
// Target server application path
const TARGET_SERVER_APP_PATH = `/home/${TARGET_SERVER_USER}/child-ensign`;
// Your repository
const REPO = 'git@gitlab.com:manas842/child-ensign.git';

module.exports = {
  apps: [
    {
      name: 'Childensign Base',
      script: './index.js',
      env: {
        NODE_ENV: 'production',
        merge_logs: true,
        vizion: true,
        autorestart: true,
        watch: false,
        instance_var: 'NODE_APP_INSTANCE',
        pmx: true,
        automation: true,
        treekill: true,
        windowsHide: true,
        kill_retry_time: 100,
        write: true,
      },
      merge_logs: true,
      vizion: true,
      autorestart: true,
      watch: false,
      instance_var: 'NODE_APP_INSTANCE',
      pmx: true,
      automation: true,
      treekill: true,
      windowsHide: true,
      kill_retry_time: 100,
      write: true,
    },
  ],
  deploy: {
    stage: {
      user: TARGET_SERVER_USER,
      host: TARGET_SERVER_HOST,
      ref: 'main',
      repo: REPO,
      ssh_options: 'StrictHostKeyChecking=no',
      'pre-deploy': `[ ! -d ${TARGET_SERVER_APP_PATH}/current/.git ] && mkdir -p ${TARGET_SERVER_APP_PATH}/source && git clone ${REPO} ${TARGET_SERVER_APP_PATH}/source;`,
      path: TARGET_SERVER_APP_PATH,
      'post-deploy': 'git reset --hard && git pull origin main && npm i && pm2 startOrGracefulReload pm2-ecosystem.config.js --env production && pm2 save',
    },
  },
};
